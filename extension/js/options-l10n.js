/*
 * File: options-l10n.js
 *
 * Copyright (c) 2019, Iván Ruvalcaba <ivanruvalcaba[at]protonmail[dot]com>
 * Author: Iván Ruvalcaba <ivanruvalcaba[at]protonmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

const getCurrentTargetLang = () => {
    let targetLang = browser.storage.local.get('lang');

    targetLang.then((res) => {
        if (res.lang === 'en') {
            document.getElementById('current-target-lang').textContent = browser.i18n.getMessage('optionsENLangMessage');
        } else if (res.lang === 'de') {
            document.getElementById('current-target-lang').textContent = browser.i18n.getMessage('optionsDELangMessage');
        } else if (res.lang === 'fr') {
            document.getElementById('current-target-lang').textContent = browser.i18n.getMessage('optionsFRLangMessage');
        } else if (res.lang === 'es') {
            document.getElementById('current-target-lang').textContent = browser.i18n.getMessage('optionsESLangMessage');
        } else if (res.lang === 'pt') {
            document.getElementById('current-target-lang').textContent = browser.i18n.getMessage('optionsPTLangMessage');
        } else if (res.lang === 'it') {
            document.getElementById('current-target-lang').textContent = browser.i18n.getMessage('optionsITLangMessage');
        } else if (res.lang === 'nl') {
            document.getElementById('current-target-lang').textContent = browser.i18n.getMessage('optionsNLLangMessage');
        } else if (res.lang === 'pl') {
            document.getElementById('current-target-lang').textContent = browser.i18n.getMessage('optionsPLLangMessage');
        } else if (res.lang === 'ru') {
            document.getElementById('current-target-lang').textContent = browser.i18n.getMessage('optionsRULangMessage');
        } else {
            document.getElementById('current-target-lang').textContent = browser.i18n.getMessage('optionsNoneLangMessage');
        }
    });

};

const internationalizeOptionsPageStrings = () => {
    document.getElementById('options-title').textContent = browser.i18n.getMessage('optionsPageTitle');
    document.getElementById('target-lang-description').textContent = browser.i18n.getMessage('optionsSelectLangDescription');
    getCurrentTargetLang();
    document.getElementById('target-lang-options').textContent = browser.i18n.getMessage('optionsSelectLangMessage');
    document.getElementById('en-lang').textContent = browser.i18n.getMessage('optionsENLangMessage');
    document.getElementById('de-lang').textContent = browser.i18n.getMessage('optionsDELangMessage');
    document.getElementById('fr-lang').textContent = browser.i18n.getMessage('optionsFRLangMessage');
    document.getElementById('es-lang').textContent = browser.i18n.getMessage('optionsESLangMessage');
    document.getElementById('pt-lang').textContent = browser.i18n.getMessage('optionsPTLangMessage');
    document.getElementById('it-lang').textContent = browser.i18n.getMessage('optionsITLangMessage');
    document.getElementById('nl-lang').textContent = browser.i18n.getMessage('optionsNLLangMessage');
    document.getElementById('pl-lang').textContent = browser.i18n.getMessage('optionsPLLangMessage');
    document.getElementById('ru-lang').textContent = browser.i18n.getMessage('optionsRULangMessage');
    document.getElementById('save-options').textContent = browser.i18n.getMessage('optionsSaveOptionsButton');
};

internationalizeOptionsPageStrings();
