/*
 * File: blank-l10n.js
 *
 * Copyright (c) 2019, Iván Ruvalcaba <ivanruvalcaba[at]protonmail[dot]com>
 * Author: Iván Ruvalcaba <ivanruvalcaba[at]protonmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

// FIXME: Remove innerHTML usage because is unsafe.
const internationalizeSidebarContentStrings = () => {
    document.getElementById('title-extensions-name').textContent = browser.i18n.getMessage('extensionName');
    document.getElementById('welcome').innerHTML = browser.i18n.getMessage('sidebarWelcomeMessage');
    document.getElementById('title-getting-started').textContent = browser.i18n.getMessage('sidebarGettingStarted');
    document.getElementById('getting-started').textContent = browser.i18n.getMessage('sidebarGettingStartedMessage');
    document.getElementById('step-one').innerHTML = browser.i18n.getMessage('sidebarGettingStartedStepOneMessage');
    document.getElementById('step-two').textContent = browser.i18n.getMessage('sidebarGettingStartedStepTwoMessage');
    document.getElementById('step-three').innerHTML = browser.i18n.getMessage('sidebarGettingStartedStepThreeMessage');
    document.getElementById('title-sponsoring').textContent = browser.i18n.getMessage('sidebarSponsoring');
    document.getElementById('sponsoring').innerHTML = browser.i18n.getMessage('sidebarSponsoringMessage');
    document.getElementById('title-support').textContent = browser.i18n.getMessage('sidebarSupport');
    document.getElementById('support').innerHTML = browser.i18n.getMessage('sidebarSupportMessage');
    document.getElementById('title-license').textContent = browser.i18n.getMessage('sidebarLicense');
    document.getElementById('license').innerHTML = browser.i18n.getMessage('sidebarLicenseMessage');
};

internationalizeSidebarContentStrings();
