/*
 * File: options.js
 *
 * Copyright (c) 2019, Iván Ruvalcaba <ivanruvalcaba[at]protonmail[dot]com>
 * Author: Iván Ruvalcaba <ivanruvalcaba[at]protonmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

const saveOptions = (e) => {
    browser.storage.local.set({
        lang: document.querySelector('#target-lang').value
    });
    e.preventDefault();
};

document.querySelector('form').addEventListener('submit', saveOptions);
