/*
 * File: background.js
 *
 * Copyright (c) 2019, Iván Ruvalcaba <ivanruvalcaba[at]protonmail[dot]com>
 * Author: Iván Ruvalcaba <ivanruvalcaba[at]protonmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

const contextMenuEntryTitle = browser.i18n.getMessage('contextMenuTitle');

browser.menus.create({
    id: 'open-link-in-sidebar',
    title: contextMenuEntryTitle,
    contexts: ['selection']
});

browser.menus.onClicked.addListener((info) => {
    let targetLang = browser.storage.local.get('lang');

    targetLang.then((res) => {
        let url = 'https://www.deepl.com/translator#en/' + res.lang + '/' + info.selectionText;

        browser.sidebarAction.open();
        browser.sidebarAction.setPanel({
            panel: url
        });
    });
});
