# Deepl Translate in sidebar

Adds the option to translate the selected text with DeepL in the context menu and shows the result of the translation in the sidebar.

## Development

**First step:** Fork it:

```sh
git clone https://gitlab.com/ivanruvalcaba/deepl-translate-in-sidebar.git
```

**Second step:** Install all project dependecies/tools (web development only):

> Replace `yarn` with `npm` if is your case.

```sh
yarn install
```

Then run:

+ `yarn start:firefox`: Launch Firefox in dev mode, builds and then temporarily installs an extension, so it can be tested. By default, watches extension source files and reload the extension in each target as files change.
+ `yarn lint`: Reports errors in the _extension manifest_ or other source code files.
+ `yarn build`: Packages the extension into a `.zip` file, ignoring files that are commonly unwanted in packages, such as `.git` and other artifacts. The name of the `.zip` file is taken from the name field in the _extension manifest_.

## Author

Copyright (c) 2019 — [Iván Ruvalcaba](https://keybase.io/ivanruvalcaba).

## Disclaimer

This extension is not affiliated with DeepL or Lionbridge Inc.

## License

This project is licensed under the [Mozilla Public License Version 2.0](https://www.mozilla.org/en-US/MPL/2.0/).